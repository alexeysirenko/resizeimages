import java.io.File
import java.lang.Throwable

import com.sksamuel.scrimage.nio.{PngWriter, JpegWriter}
import com.typesafe.scalalogging.Logger
import org.apache.commons.io.FilenameUtils
import com.sksamuel.scrimage._
import org.slf4j.LoggerFactory

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Oleksiy on 16.04.2016.
 */
object Main {

  val logger = Logger(LoggerFactory.getLogger("Application"))

  def isRatio(s: String) = { s.forall(_.isDigit) && Range(1, 99).contains(s.toInt)}

  def isFiles(fileNames: Seq[String]) = { fileNames.forall({ fileName =>
    val file = new File(fileName)
    file.isFile && file.exists()
  })}

  def main(args: Array[String]): Unit = {

    args match {
      case Array(ratio, fileNames @ _*) if isRatio(ratio) && isFiles(fileNames) =>
        val ratioValue = ratio.toInt
        val files = fileNames.map(new File(_))
        convertFiles(ratioValue, files)
      case _ =>
        logger.debug("Invalid arguments")
    }
  }

  def convertFiles(ratio: Int, files: Seq[File]) {
    logger.debug(s"Arguments: $ratio% ${files.mkString}")

    files.foreach({inputFile =>
        val path = inputFile.getCanonicalPath

        val fileWriter = FilenameUtils.getExtension(path) match {
          case "jpg" => (JpegWriter(), "jpg")
          case _ => (PngWriter(), "png")
        }
        val outputPath = FilenameUtils.removeExtension(path) +
          s"_resized_$ratio." + fileWriter._2
        val outputFile = new File(outputPath)
        Image.fromFile(inputFile).scale(ratio / 100.0).output(outputFile)(fileWriter._1)
        logger.debug(s"Done resizing $outputFile")
      })
  }
}
